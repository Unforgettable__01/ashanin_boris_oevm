package com.company;
import java.util.Scanner;
public class Main
{
    public static int res=0;
    public static void ConvertTo10(char [] value, int _startSS)
    {
        char[] simValue = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        int[] chValue = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};


        for (int i=0;i<value.length;i++)
        {
            for (int j=0;j<simValue.length;j++)
            {
                if (value[i]==simValue[j])
                {
                    res+=chValue[j]*Math.pow(_startSS,value.length-i-1);
                }
            }
        }
    }


    /*
     Функция в которой я перевожу число в конечную систему счисления из 10-ой сс, реализовал ее
     при помощи switch, если необходимо могу переделать через  массивы как в предыдущей функции
     */
    public static  void ConvertToX(int osn , int chislo) {
        String otvet = "";
        if (osn < 2 || osn > 16) {
            System.out.println("Система счисления не подходит");
            return;
        }
        int ost;
        do {
            ost = chislo % osn;
            chislo = chislo / osn;
            if (ost < 10) {
                otvet = ost + otvet;
            } else {
                char res;
                switch (ost) {
                    case 16:
                        res = 'G';
                        otvet = res + otvet;

                        break;

                    case 15:
                        res = 'F';
                        otvet = res + otvet;

                        break;
                    case 14:
                        res = 'E';
                        otvet = res + otvet;

                        break;
                    case 13:
                        res = 'D';
                        otvet = res + otvet;

                        break;
                    case 12:
                        res = 'C';
                        otvet = res + otvet;

                        break;
                    case 11:
                        res = 'B';
                        otvet = res + otvet;

                        break;
                    case 10:
                        res = 'A';
                        otvet = res + otvet;

                        break;
                }
            }
        }
        while (chislo / osn != 0) ;
        ost = chislo % osn;
        chislo = chislo / osn;
        if (ost < 10) {
            otvet = ost + otvet;
            System.out.print(otvet);
        } else {
            char res;
            switch (ost) {
                case 16:
                    res = 'G';
                    otvet = res + otvet;
                    System.out.print(otvet);
                    break;

                case 15:
                    res = 'F';
                    otvet = res + otvet;
                    System.out.print(otvet);
                    break;
                case 14:
                    res = 'E';
                    otvet = res + otvet;
                    System.out.print(otvet);
                    break;
                case 13:
                    res = 'D';
                    otvet = res + otvet;
                    System.out.print(otvet);
                    break;
                case 12:
                    res = 'C';
                    otvet = res + otvet;
                    System.out.print(otvet);
                    break;
                case 11:
                    res = 'B';
                    otvet = res + otvet;
                    System.out.print(otvet);
                    break;
                case 10:
                    res = 'A';
                    otvet = res + otvet;
                    System.out.print(otvet);
                    break;
            }
        }
    }
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите начальную систему счисления");
        int _startSS=scanner.nextInt();

        System.out.println("Введите систему счисления в которую будем перводить от 2 до 16");
        Scanner scan = new Scanner(System.in);
        int osn = scan.nextInt();

        System.out.println("Введите число для перевода");
        Scanner s = new Scanner(System.in);
        char[] value = s.nextLine().toCharArray();

        ConvertTo10(value ,_startSS);
        ConvertToX( osn,res);

    }
}
